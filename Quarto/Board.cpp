#include "Board.h"




std::optional<Piece> Board::operator[](const Position & positions) const
{
	const auto&[lineIndex, columnIndex] = positions; //structured binding;
	return m_elements[lineIndex*kMatrixDimension + columnIndex];
}

std::optional<Piece>& Board::operator[](const Position & positions)
{
	const auto&[lineIndex, columnIndex] = positions;
	return m_elements[lineIndex*kMatrixDimension + columnIndex];
}

std::ostream & operator<<(std::ostream & os, const Board & board)
{
	Board::Position position;
   auto&[lineIndex, columnIndex] = position; //structured binding;

	for (lineIndex = 0; lineIndex < Board::kMatrixDimension; ++lineIndex)
	{
		for (columnIndex = 0; columnIndex < Board::kMatrixDimension; ++columnIndex)

		{
			if (board[position])   //board[position].has_value();
				os << *board[position];//board[position].value();
			else
				os << "____";
			os << ' ';
		}
		os << std::endl;
	}
		return os;
}
//clasa care sa retina 16 piese unice;extragere piese