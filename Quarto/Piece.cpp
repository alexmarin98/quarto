#include "Piece.h"


std::ostream & operator<<(std::ostream & os, const Piece & piece)
{
	os << static_cast<int>(piece.m_body);
	os << static_cast<int>(piece.m_color);
	os << static_cast<int>(piece.m_height);
	os << static_cast<int>(piece.m_shape);
	return os;
}

Piece::Piece(Body body, Color color, Height height, Shape shape) :
	m_body(body),
	m_color(color),
	m_height(height),
	m_shape(shape)
{
	static_assert(sizeof(*this) == 1, "Size of Piece is not 1");
}

Piece::Body Piece::GetBody() const
{
	return m_body;
}

Piece::Color Piece::GetColor() const
{
	return m_color;
}

Piece::Height Piece::GetHeight() const
{
	return m_height;
}

Piece::Shape Piece::GetShape() const
{
	return m_shape;
}
