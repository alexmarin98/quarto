#pragma once
#include <array>
#include <optional>
#include "Piece.h"
//typedef std::pair<uint8_t, uint8_t> Position;
class Board
{
public:
	using Position = std::pair<uint8_t, uint8_t>;
public:
	//Getter
	std::optional<Piece> operator[](const Position& positions) const;

	//setter
	std::optional<Piece>& operator[](const Position& positions);
	friend std::ostream& operator<<(std::ostream& os, const Board & board);

private:
	static const uint8_t kMatrixDimension = 4;
	static const uint8_t kNumberOfElements = kMatrixDimension*kMatrixDimension;
	std::array<std::optional<Piece>,kNumberOfElements> m_elements;
	
};

