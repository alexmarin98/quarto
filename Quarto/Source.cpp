#include "Piece.h"
#include "Board.h"
#include <iostream>

int main()
{
	Piece piece(Piece::Body::Full, Piece::Color::Light, Piece::Height::Tall, Piece::Shape::Square);
	std::cout << piece << std::endl;
	Board board;
	std::cout << board<<std::endl;
//	board[Board::Position(1, 1)] = piece;
	//board[std::make_pair(1, 1)] = piece;
	board[{1, 1}] = piece;  //lista de initializare
	std::cout << board << std::endl;



	return 0;
}
